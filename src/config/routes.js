const express = require('express')
const bcrypt = require('bcryptjs')
const multerConfig = require('../config/multer')
const multer = require('multer')


module.exports = function (server) {

    // Definir URL base para todas as rotas 
    const router = express.Router()
    server.use('/api', router)

    //Rotas de fotos da galeria
    const Galeria = require('../api/controller/ServicoDeGaleria')
    Galeria.register(router, '/galeria')

    //Rotas de slides
    const Slide = require('../api/controller/ServicoDeSlide')
    router.post('/slides', multer(multerConfig).single('file'), Slide.createSlide)
    router.get('/slides', Slide.showSlides)
    router.delete('/slides/:id', Slide.deleteSlide)

    //Rotas de endereco
    const Endereco = require('../api/controller/ServicoDeEndereco')
    Endereco.register(router, '/endereco')

    //Rotas de produtos
    const Product = require('../api/controller/ServicoDeProduto')
    Product.register(router, '/produtos')

    //Rota para gravação e envio de contatos do site
    const Email = require('../api/controller/ServicoDeEmail')
    Email.register(router, '/send_contato')

    //rotas de autenticação e usuários
    const ServicoAdm = require('../api/controller/ServicoAdm')
    router.post('/registrar', ServicoAdm.createUser)
    router.get('/usuarios', ServicoAdm.listUsers)
    router.post('/login', ServicoAdm.login)
    router.delete('/usuarios/:id', ServicoAdm.deleteUser)
    router.get('/usuarios/:id', ServicoAdm.consultUser)

    //rota de pessoas
    const Pessoa = require('../api/controller/ServicoDePessoas')
    Pessoa.register(router, '/pessoas')

    //rota para upload de arquivos
    const ServicoDeUpload = require('../api/controller/ServicoDeUpload')
    router.post('/upload', multer(multerConfig).single('file'), ServicoDeUpload.uploadFile)
}