const nodemailer = require('nodemailer')

module.exports = {
    async sendEmail(body) {
        const transporte = await nodemailer.createTransport({
            host: process.env.URL_HOST_SMTP,
            port: 587,
            secure: false,
            auth: {
                user: process.env.URL_USER_EMAIL,
                pass: process.env.URL_USER_PASSWORD
            }
        })

        const email = {
            from:   process.env.URL_USER_EMAIL,
            to:     process.env.URL_EMAIL_TO,
            subject: 'Novo contato recebido via site www.goiafio.com.br',
            html: body
        }

        await transporte.sendMail(email, (err, info, res) => {
            if (err) {
                throw err + ' erro no envio';
            }
            res.status(200).send({ success: 'testes'})
        })
    }
}

