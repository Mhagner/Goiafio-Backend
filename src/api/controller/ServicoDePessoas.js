const Pessoa = require('../model/pessoa')

Pessoa.methods(['post', 'get', 'put', 'delete'])
Pessoa.updateOptions({ new: true, runValidators: true })

module.exports = Pessoa
