const Upload = require('../model/upload')

module.exports = {
    async uploadFile(req, res) {

        const { originalname: name, size, filename: key } = req.file

        const upload = await Upload.create({
            name,
            size,
            key,
            url: ''
        }, function (err, upload) {
            if (err) return res.status(500).send({ msg: 'erro ao processar a operação ' + err })

            res.status(200).json(upload)
        })
    }
}
