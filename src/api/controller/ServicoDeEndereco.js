const Endereco = require('../model/endereco')

Endereco.methods(['get', 'post', 'put', 'delete'])
Endereco.updateOptions({ new: true, runValidators: true })

module.exports = Endereco