const Email = require('../model/email')
const emailServices = require('../services/emailServices')

Email.methods(['get', 'post', 'delete'])

Email.after('post', enviaEmail)

function enviaEmail(req, res, next){
    var html = 
            `<h2>Novo contato recebido</h2><br/><br/>
            <b><strong>Nome: </strong> ${req.body.name}</b><br/><br/>
            <b><strong>Assunto: </strong> ${req.body.assunto}</b><br/><br/>
            <b><strong>Telefone: </strong> ${req.body.phone}</b><br/><br/>
            <b><strong>Email: </strong> ${req.body.email}</b><br/><br/>
            <b><strong>Mensagem: </strong> ${req.body.message}</b>
            `
            
    emailServices.sendEmail(html)
    
    res.status(200).send({ success: 'E-mail  enviado com sucesso'})

    next()
}

module.exports = Email