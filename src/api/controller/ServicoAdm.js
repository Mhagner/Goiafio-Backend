const User = require('../model/user')
const bcrypt = require('bcryptjs')


const messageUserNotFound = 'Usuário não encontrado'
const messageProcessError = 'Erro no processamento'
const messageUserAndOrInvalid = 'Usuário e/ou senha inválidos'
const messageCreateNewUser = 'Novo usuário criado com sucesso!'
const messageUserDeleted = 'Usuário excluido com sucesso!'

module.exports = {
    async listUsers(req, res) {
        const user = await User.find({})

        return res.json(user)
    },

    async consultUser(req, res) {
        const user = await User.findById(
            req.params.id,
            function (err, user) {
                if (err) return res.status(500).send({ message: messageProcessError })

                if (!user) return res.status(404).send({ message: messageUserNotFound })

                return res.json(user)

            })
    },

    async createUser(req, res) {
        const hashPassword = bcrypt.hashSync(req.body.password, 8)
        
        const { name, email, password } = req.body

        const user = await User.create({
            name,
            email,
            password
        },
            function (err, user) {
                if (err) return res.status(500).send({
                    auth: false,
                    message: messageProcessError
                })

                res.status(200).send({
                    auth: true,
                    message: messageCreateNewUser
                })
            })
    },


    async login(req, res) {
        const login = await User.findOne({
            email: req.body.email
        }, function (err, user) {

            if (err) return res.status(500).send({
                auth: false,
                message: messageProcessError
            })

            if (!user) return res.status(404).send({
                auth: false,
                message: messageUserAndOrInvalid
            })

            const passwordIsValid = bcrypt.compareSync(req.body.password, user.password)

            if (!passwordIsValid) {
                return res.status(404).send({
                    auth: false,
                    message: messageUserAndOrInvalid
                })
            } else {
                return res.status(200).send({
                    auth: true
                })
            }

        })
    },
    async deleteUser(req, res) {
        await User.findByIdAndRemove(req.params.id,
            function (err, user) {
                if (err) return res.status(500).send({
                    message: messageProcessError
                })

                res.status(200).send({
                    message: messageUserDeleted
                })
            })
    }
}