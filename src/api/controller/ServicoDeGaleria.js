const Galeria = require('../model/galeria')

Galeria.methods(['get', 'post', 'put', 'delete'])
Galeria.updateOptions({ new: true, runValidators: true })

module.exports = Galeria