const Slide = require('../model/slide')

module.exports = {

    async createSlide(req, res) {

        const { originalname: name, key, location: url = '' } = req.file
        const { titulo, subtitulo, label } = req.body

        const slide = await Slide.create({
            titulo,
            name,
            key,
            url,
            subtitulo,
            link: `/${req.body.link}`,
            label
        },

            function (err, slide) {

                if (err) return res.status(500).send({ msg: 'erro: ' + err })

                res.json(slide)
            })
    },

    async showSlides(req, res) {
        const slide = await Slide.find({})

        return res.json(slide)
    },

    async deleteSlide(req, res) {
        await Slide.findByIdAndRemove(req.params.id,
            function (err, slide) {
                if (err) return res.status(500).send({
                    message: "erro: " + err
                })

                res.status(200).send({
                    message: "slide excluído com sucesso!"
                })
            })
    }
}
