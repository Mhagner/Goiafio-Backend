const restful = require('node-restful')
const mongoose = restful.mongoose

const pessoaSchema = new mongoose.Schema({
    nome: { type: String, required: true },
    cpf: { type: Number, required: true },
    idade: { type: Number, required: true },
})

module.exports = restful.model('Pessoa', pessoaSchema)