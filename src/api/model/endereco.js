const restful = require('node-restful')
const mongoose = restful.mongoose

const enderecoSchema =  new mongoose.Schema({
    titulo: { type: String, required: true },
    endereco: { type: String, required: true },
    telefone: { type: String, required: true },
    site: { type: String, required: true },
    email: { type: String, required: true }
})

module.exports = restful.model('Endereco', enderecoSchema)