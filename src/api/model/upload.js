const mongoose = require('mongoose')

const uploadSchema = new mongoose.Schema({
    name: { type: String, required: true },
    size: { type: Number, required: true },
    key: { type: String, required: true },
    url: { type: String },
    createdAd: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Upload', uploadSchema)