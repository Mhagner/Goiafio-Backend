var restFul = require('node-restful')
var mongoose = restFul.mongoose

const galeriaSchema = new mongoose.Schema({
    title: { type: String, required: true },
    bigImage: { type: String, required: true },
    smallImage: { type: String, required: true },
    text: { type: String, required: true }
})

module.exports = restFul.model('Galeria', galeriaSchema )