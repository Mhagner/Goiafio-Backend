const restful = require('node-restful')
const mongoose = restful.mongoose

const emailSchema = new mongoose.Schema({
    name: { type: String, required: true },
    assunto: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    message: { type: String, required: true }
})

module.exports = restful.model('Email', emailSchema)