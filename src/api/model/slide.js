var mongoose = require('mongoose')

const slideSchema = new mongoose.Schema({
    titulo: { type: String, required: true },
    name: { type: String, required: true },
    key: { type: String, required: true },
    url: { type: String },
    subtitulo: { type: String, required: true },
    link: { type: String, required: true },
    label: { type: String, required: true }
})

/*slideSchema.pre('save', function (next) {
    if (!this.url) {
        this.url = `${process.env.APP_URL}/files/${this.key}`
        next()
    }
})*/

module.exports = mongoose.model('Slide', slideSchema)